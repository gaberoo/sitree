#ifndef __TREE_H__
#define __TREE_H__

#include "expoTree.h"
#include <vector>
#include <string>
using namespace std;

class Tree {
public:
  Tree() : extant(0), maxExtant(0), fn("") {}
  Tree(const char* fn) { read(string(fn)); }
  Tree(string fn) { read(fn); }
  Tree(const Tree& T) 
    : times(T.times), ttypes(T.ttypes), extant(T.extant), maxExtant(T.maxExtant)
  {}
  virtual ~Tree() {}
  
  inline void read(string filename) { 
    fn = filename;
    readTimes(fn,times,ttypes,extant,maxExtant); 
  }
  inline double* ti() { return times.data(); }
  inline int* tt() { return ttypes.data(); }

  inline double survival(int N, double beta, double mu, double psi, double rho,
      int SImodel = 1, int vflag = 0, int rescale = 1) {
    double fx(-INFINITY);
    if (N == 0) 
      fx = infTreeSurvival(beta,mu,psi,rho,times.back());
    else 
      fx = expoTreeSurvival(N,beta,mu,psi,rho,times.back(),extant,SImodel,vflag,rescale);
    return fx;
  }

  inline double likelihood(int N, double beta, double mu, double psi, 
      double rho, int SImodel = 1, int vflag = 0, int rescale = 1, int surv = 1) {
    double fx(-INFINITY);
    if (N == 0 || SImodel == 0) {
      fx = infTreeEval(beta,mu,psi,rho,times,ttypes,extant,SImodel,0,vflag);
    } else {
      fx = expoTreeEval(N,beta,mu,psi,rho,times,ttypes,extant,SImodel,vflag,rescale);
    }
    if (surv) fx -= survival(N,beta,mu,psi,rho);
    return fx;
  }

  inline double likelihood(const vector<double>& vars,
      int SImodel = 1, int vflag = 0, int rescale = 1, int surv = 1) {
    return likelihood((int)vars[4],vars[0],vars[1],vars[2],vars[3],
                      SImodel,vflag,rescale,surv);
  }

  vector<double> times;     /* event times */
  vector<int> ttypes;       /* branching event type */
  int extant;               /* extant branches at t=0 */
  int maxExtant;            /* maximum number of extant branches */
  string fn;                /* tree file name */
};

#endif
