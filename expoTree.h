#ifndef __EXPOTREE_H__
#define __EXPOTREE_H__

#include <cstdlib>
#include <cstdio>
#include <string>
#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

#include <gsl/gsl_sort_double.h>
#include <gsl/gsl_permute.h>
#include <gsl/gsl_permute_int.h>

extern "C" {
  void rExpoTree(int* RN, int* Rki, double* Rbeta, double* Rmu,
      double* Rpsi, int* Rn, double* times, int* ttypes, 
      double* p, double* t0, int* RSImodel, int* Rvflag, int* Rrescale);

  void expoTree(int n, double* times, int* ttypes, 
      double* p, int wrklen, double* wrk, int iwrklen, int* iwrk);
}

void readTimes(string fn, vector<double>& times, vector<int>& ttypes,
               int& extant, int& maxExtant);

double expoTreeEval(int N, double beta, double mu, double psi, double rho,
    vector<double>& times, vector<int>& ttypes, 
    int extant, int SI, int vf, int rs);

double expoTreeSurvival(int N, double beta, double mu, double psi, double rho,
    double torig, int extant, int SI, int vf, int rs);

// ===========================================================================

inline double bdss_q(double t, double c1, double c2) {
  double q((1-c2)*exp(-t*c1/2.0) + ((1+c2)*exp(t*c1/2)));
  return 0.25*q*q;
}

double infTreeSurvival(double beta, double mu, double psi, double rho, 
    double torig);

double infTreeEval(double beta, double mu, double psi, double rho,
    const vector<double>& times, const vector<int>& ttypes, 
    int extant, int SI, int survival, int vf);


#endif
