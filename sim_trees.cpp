#include <iostream>
#include <vector>
#include <list>
#include <cmath>
#include <typeinfo>
#include <string>
#include <sstream>
#include <algorithm>
#include <getopt.h>
using namespace std;

#ifndef RLIB
#include <mygsl/GSLRng.h>
#include <gsl/gsl_math.h>
class Rng {
  public:
    Rng() {}
    virtual ~Rng() {}
    inline void set(unsigned long seed) { rng.set(seed); }
    inline double uniform() { return rng.uniform(); }
    inline int uniform_int(int max) { return rng.uniform_int(max); }
    myGSL::Rng rng;
};
#else
#include <R.h>
#include <Rmath.h>
class Rng {
  public:
    Rng() { GetRNGState(); }
    virtual ~Rng() {}
    inline void set(unsigned long seed) { return; }
    inline double uniform() { return rand_unif(); }
    inline int uniform_int(int max) { return (int) (max*uniform()); }
};
#endif

// ===========================================================================

struct _pars {
  int N;
  double beta;
  double mu;
  double psi;
  double t;
} pars;

// ===========================================================================

double lambda(int I) { return pars.beta*(1.-1.*I/pars.N); }

// ===========================================================================

struct Individual {
  Individual(double it = 0.0, int p = 0) 
    : itime(it), dtime(-1.0), stime(-1.0), parent(p), id(-1)
  {}

  virtual ~Individual() {}

  double itime;  // infection time
  double dtime;  // death time
  double stime;  // sample time
  int parent;    // parent individual
  int id;        // my id
  vector<int> children;
};

// ===========================================================================

int getBranchingTimes(vector<double>& times, int subclade, 
    const vector<Individual>& pop) 
{
  int ret = 0;
  // check if the leaf is sampled
  if (pop[subclade].stime >= 0.0) ret = 1;
  // check if at least one of the children is sampled
  for (int i(pop[subclade].children.size()-1); i >= 0; --i) {
    if (getBranchingTimes(times,pop[subclade].children[i],pop)) {
      if (ret) times.push_back(pop[pop[subclade].children[i]].itime);
      else ret = 1;
    }
  }
  return ret;
}

// ===========================================================================

string newickString(int subclade, const vector<Individual>& pop, 
    bool sampled = false) 
{
  ostringstream str;
  double t(pars.t);
  double dtime(0.0);
  if (pop[subclade].stime >= 0.0) dtime = pop[subclade].stime;
  else if (pop[subclade].dtime >= 0.0) dtime = pop[subclade].dtime;
  else dtime = t;
  if (pop[subclade].children.size() == 0) {
    str << "'" << subclade;
    if (pop[subclade].stime >= 0.0) str << "+";
    str << "':";
    str << dtime-pop[subclade].itime;
  } else {
    t = dtime;
    for (size_t i(0); i < pop[subclade].children.size(); ++i) str << "(";
    str << "'" << subclade;
    if (pop[subclade].stime >= 0.0) str << "+";
    str << "'";
    for (int i(pop[subclade].children.size()-1); i >= 0; --i) {
      str << ":" << t-pop[pop[subclade].children[i]].itime;
      str << "," << newickString(pop[subclade].children[i],pop,sampled) << ")";
      // str << "'" << subclade << "->" << pop[subclade].children[i] << "'";
      t = pop[pop[subclade].children[i]].itime;
    }
    str << ":" << t-pop[subclade].itime;
  }
  return str.str();
}

void printHelp() {
  printf("sim_trees : Simulate transmission trees under epidemiological models.\n\n");
  printf("usage: sim_trees \n");
  printf("-N <population size> ");
  printf("-b <beta> -m <mu> -p <psi> [-tnAh] [-s <samples>] [-O <infecteds>] [-S <seed>]\n\n");
  printf("  N : total population size\n");
  printf("  b : infection rate 'beta'\n");
  printf("  m : recovery rate 'mu'\n");
  printf("  p : sampling rate 'psi'\n");
  printf("  d : output mode\n");
  printf("       1 = full tree in Nexus format\n");
  printf("       2 = branching times (default)\n");
  printf("       3 = epidemic curve\n");
  printf("       0 = all\n");
  printf("  n : output tree in Newick format\n");
  printf("  A : output both the branching times and the tree\n");
  printf("  S : set the random number seed\n");
  printf("  s : exit simulation when s individuals have been sampled\n");
  printf("  O : minimum number of infecteds considered an outbreak\n");
  printf("  h : output this help message\n");
}

// ===========================================================================

double sim_trees(vector<Individual>& pop, list<int>& inf, vector<int>& samples,
    int max_samples, double max_time, int min_outbreak, Rng& rng, int output) {
  double t(0.0);
  
  double totalRate(0.0);
  double nextEventTime(0.0);
  double r(0.0);
  double l(0.0);

  int S(pars.N-1); // susceptibles
  int I(1);        // infecteds
  int J(0);        // sampleds
  int node(0);

  list<int>::iterator infit;

  int max_tries = 1000;
  int tries = 0;
  while (tries++ < max_tries) {
    pop.clear();
    inf.clear();
    samples.clear();
    t = 0.0;
    nextEventTime = 0.0;
    r = 0.0;
    l = 0.0;
    S = pars.N-1;
    I = 1;
    J = 0;
    pop.push_back(Individual(0.0,-1));
    inf.push_back(0);
    while (I > 0 && samples.size() < max_samples && t < max_time) {
      if (pars.psi <= 0.0 && I >= max_samples) {
        for (infit = inf.begin(); infit != inf.end(); ++infit) {
          pop[*infit].stime = t;
          samples.push_back(*infit);
        }
        break;
      }
      if (pars.N > 0) l = lambda(I);
      else l = pars.beta;
      totalRate = l*I + (pars.mu+pars.psi)*I;
      nextEventTime = t-log(rng.uniform())/totalRate;
      r = rng.uniform();
      node = rng.uniform_int(I);
      infit = inf.begin();
      advance(infit,node);
      if (r < l*I/totalRate) {
        // BRANCHING EVENT
        pop.push_back(Individual());
        pop.back().itime = nextEventTime;
        pop.back().parent = *infit;
        pop.back().id = pop.size()-1;
        inf.push_back(pop.size()-1);
        pop[*infit].children.push_back(pop.size()-1);
        ++I;
      } else {
        // REMOVAL EVENT
        if (r < (l*I+pars.mu*I)/totalRate) {
          pop[*infit].dtime = nextEventTime;
        } else {
          pop[*infit].stime = nextEventTime;
          samples.push_back(*infit);
          ++J;
        }
        inf.erase(infit);
        --I;
      }

      if (output == 3 || output == 0) cout << t << " " << I << " " << J << endl;

      t = nextEventTime;
    }
    if (samples.size() >= min_outbreak) break;
  }

  // if (samples.size() < min_outbreak) t = 0.0;
  return t;
}
// ===========================================================================

void to_array(double t, vector<Individual>& pop, vector<int>& samples, 
    int len, double* x, int* xtype) {
  vector<double> times;
  getBranchingTimes(times,0,pop);
  times.push_back(0.0);
  int j(0);
  if (len >= times.size() + samples.size()) {
    for (int i(0); i < times.size(); ++i) {
      x[j] = t-times[i];
      xtype[j] = 1;
      ++j;
    }
    for (int i(0); i < samples.size(); ++i) {
      x[j] = t-pop[samples[i]].stime;
      xtype[j] = 0;
      ++j;
    }
  }
}

// ===========================================================================

extern "C" {
  int RSimTrees(int* N, double* beta, double* mu, double* psi, int* S,
      int* max_samples, int* min_outbreak, double* max_time,
      int* maxlen, double* times, int* ttypes);
}

int RSimTrees(int* N, double* beta, double* mu, double* psi, int* S,
      int* max_samples, int* min_outbreak, double* max_time,
      int* maxlen, double* times, int* ttypes) {
  Rng rng;

  pars.N    = *N;
  pars.beta = *beta;
  pars.mu   = *mu;
  pars.psi  = *psi;

  int seed = time(NULL) + *S;
  if (*max_time < 0) *max_time = INFINITY;

  vector<Individual> pop;
  list<int> inf;
  vector<int> samples;

  double t = sim_trees(pop,inf,samples,*max_samples,*max_time,*min_outbreak,rng,2);
  to_array(t,pop,samples,*maxlen,times,ttypes);

  return 1;
}

// ===========================================================================

#ifndef RLIB
int main(int argc, char* argv[]) {
  Rng rng;

  pars.N    = -1;
  pars.beta = -1.0;
  pars.mu   = -1e-3;
  pars.psi  = -1e-3;

  int c;
  opterr = 0;
  int output = 2;
  int seed = time(NULL);
  int max_samples = 100;
  int min_outbreak = 20;
  double max_time = INFINITY;

  while ((c = getopt(argc,argv,"N:b:m:p:d:S:s:O:hT:")) != -1) {
    switch (c) {
      case 'N': pars.N = atoi(optarg); break;
      case 'b': pars.beta = atof(optarg); break;
      case 'm': pars.mu = atof(optarg); break;
      case 'p': pars.psi = atof(optarg); break;
      case 'd': output = atoi(optarg); break;
      case 'S': seed += atoi(optarg); break;
      case 's': max_samples = atoi(optarg); break;
      case 'O': min_outbreak = atoi(optarg); break;
      case 'T': max_time = atof(optarg); break;
      case 'h': printHelp(); return 0;
    }
  }

  min_outbreak = GSL_MIN(max_samples,min_outbreak);

  if (pars.beta < 0.0 || pars.mu < 0.0 || pars.psi < 0.0) {
    fprintf(stderr,"Please specify beta, mu and psi.\n\n");
    return 0;
  }

  rng.set(seed);

  vector<Individual> pop;
  list<int> inf;
  vector<int> samples;

  double t = sim_trees(pop,inf,samples,max_samples,max_time,min_outbreak,rng,output);
  if (t == 0.0) {
    cerr << "Couldn't simulate outbreak." << endl;
  }

  if (output == 1 || output == 0) {
    pars.t = t;
    cout << "#NEXUS" << endl;
    cout << "begin trees;" << endl;
    cout << "tree 'simTree' = "
         << newickString(0,pop) << ";" << endl;
    cout << "end;" << endl;
  }
  if (output == 2 || output == 0) {
    printf("#  N = %d\n",pars.N);
    printf("#  b = %g\n",pars.beta);
    printf("#  m = %g\n",pars.mu);
    printf("#  p = %g\n",pars.psi);
    printf("#  S = %d\n",seed);
    printf("#  s = %d\n",max_samples);
    printf("#  O = %d\n",min_outbreak);

    int maxlen = max_samples*2;
    double* x = new double[maxlen];
    int* xt = new int[maxlen];
    to_array(t,pop,samples,maxlen,x,xt);

    vector<double> times;
    getBranchingTimes(times,0,pop);
    times.push_back(0.0);
    for (int i(0); i < times.size(); ++i) {
      cout << t-times[i] << " 1 0" << endl;
    }
    for (int i(0); i < samples.size(); ++i) {
      cout << t-pop[samples[i]].stime << " 0 " << samples[i] << endl;
    }
    delete[] x;
    delete[] xt;
  }

  return 0;
}
#endif

