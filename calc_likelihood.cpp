#include <cstdlib>
#include <cmath>
#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <getopt.h>
using namespace std;

#include "expoTree.h"
#include "Forest.h"

void printHelp();

// ===========================================================================

int N = 0;               /* total population size */
double beta = 0.0;       /* infection rate */
double mu = 0.0;         /* recovery rate */
double psi = 0.0;        /* sampling rate */
double rho = 0.0;        /* initial sampling probability */
int root = 0;            /* add a root to the tree */
int SImodel = 1;         /* use non-saturating model */
int n = 0;               /* number of times */
int vflag = 0;           /* vebose flag */
int rescale = 1;
int extant = 0;
int maxExtant = 0;
int maxEvals = 1000;
double fixedRatio = -1.0;
char jointType = 'g';

// ===========================================================================

int main(int argc, char* argv[]) {
  string fn;

  // =========================================================================

  int c;
  opterr = 0;
  while ((c = getopt(argc,argv,"N:l:vb:u:s:o:hr:R:J:")) != -1) {
    switch (c) {
      case 'N': N = atoi(optarg); break;
      case 'l': SImodel = atoi(optarg); break;
      case 'v': ++vflag; break;
      case 'b': beta = atof(optarg); break;
      case 'u': mu = atof(optarg); break;
      case 's': psi = atof(optarg); break;
      case 'o': rho = atof(optarg); break;
      case 'r': rescale = atoi(optarg); break;
      case 'R': fixedRatio = atof(optarg); break;
      case 'h': printHelp(); return 0;
      case 'J': jointType = optarg[0]; break;
    }
  }

  // =========================================================================

  if (vflag) fprintf(stderr,"Reading times...\n");

  if (optind == argc) {
    fprintf(stderr,"Please provide times file.\n");
    return 0;
  }

  Forest* T = new Forest(argc-optind,argv+optind);

  if (vflag) {
    printf("Extant species at time zero = %d\n",T->at(0)->extant);
    printf("Max extant species = %d\n",T->maxExtant);
  }

  mu = (fixedRatio > 0.0) ? psi*(1.0/fixedRatio-1.0) : mu;

  if (! SImodel) N = 0;
  vector<double> pars(5);
  pars[0] = beta;
  pars[1] = mu;
  pars[2] = psi;
  pars[3] = rho;
  pars[4] = N;
  double fx = 0.0;
  double surv = 0.0;
  double torig = 0.0;
  cout << jointType << " ";
  switch (jointType) {
    case 'j':
      surv = (*T)[0]->survival(N,beta,mu,psi,rho,SImodel,vflag,rescale);
      fx = T->jointLikelihood(pars,SImodel,vflag,rescale,0);
      cout << N << " " << beta << " " << mu << " " 
           << psi << " " << rho << " " << fx << " " << surv << endl;
      break;
    case 'm':
      fx = T->meanLikelihood(pars,SImodel,vflag,rescale);
      cout << N << " " << beta << " " << mu << " " 
           << psi << " " << rho << " " << fx << endl;
      break;
    case 'g':
    default:
      fx = T->jointLikelihood(pars,SImodel,vflag,rescale)/T->size();
      cout << N << " " << beta << " " << mu << " " 
           << psi << " " << rho << " " << fx << endl;
      break;
  }

  delete T;
  return 0;
}

// ===========================================================================

void printHelp() {
  printf("calc_likelihood: Calculate likelihood for a phylogenetic tree from an epidemic\n\n");
  printf("usage: calc_likelihood -N <population_size> -b <beta> -u <mu> -s <psi>\n");
  printf("                       -l <SImodel> [-rvh] <times_files> \n\n");
  printf("  N : starting population size\n");
  printf("  b : starting infection rate 'beta'\n");
  printf("  u : starting recovery rate 'mu'\n");
  printf("  r : rescale staring vector after each iteration\n");
  printf("  s : starting sampling rate 'psi'\n");
  printf("  l : model to use (0 = density independent; 1 = density dependent)\n");
  printf("  f : file with time samples\n");
  printf("  t : fixed parameters\n");
  printf("  v : verbose (can be used multiple times)\n");
  printf("  h : print this help message\n");
}

