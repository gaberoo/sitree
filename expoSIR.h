#ifndef __EXPOSIR_H_
#define __EXPOSIR_H_

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <string.h>

/************************************************************/ 

double dnrm2_(int* n, double* x, int* incx);
double dscal_(int* n, double* alpha, double* x, int* incx);

/************************************************************/ 

void matFuncExpmvShiftedSIR(char trans, int n1, int n2, 
                  double alpha, double* pin, double* pout);
double matFuncOneNormSIR();
double matFuncTraceSIR();
void transEventSIR(double* pin, double* pout);
void sampleEventSIR(double* pin, double* pout);

/************************************************************/ 

int cmpdbl(const void* a, const void* b);

/************************************************************/ 

inline int sirmat(int I, int R) { 
  return (I <= expo->N-R) ? R*expo->N-(R-1)*R/2 + (I-1) : -1; 
}

/************************************************************/ 

inline int ztoir(int z, int* x) {
  x[0] = 0;
  x[1] = 0;
  int nn = expo->N;
  while (z > nn) { z -= nn; ++x[1]; --nn; }
  x[0] = z;
}

/************************************************************/ 

double matFuncTraceSIR() {
  int i, r;
  double trace = 0.0;
  for (r = 0; r < expo->N; ++r) {
    for (i = expo->ki; i < expo->N-r; ++i) {
      trace += -i*(expo->lambda(i+r)+expo->psi+expo->mu);
    }
  }
  return trace;
}

/************************************************************/ 

double matRowSumSIR(int i, int r, int abs) {
  int z = sirmat(i,r);
  if (i < expo->ki) return 0.0;
  double l = expo->lambda(i+r);
  double rs = -i*(l+expo->mu+expo->psi) - expo->shift;
  if (abs) rs = fabs(rs);
  if (i < expo->N-r) 
    rs += (abs) ? fabs((i+expo->ki)*l) : (i+expo->ki)*l;
  if (i > 1 && r < expo->N-1) 
    rs += (abs) ? fabs((i-expo->ki)*expo->mu) : (i-expo->ki)*expo->mu;
  if (r > 0) 
    rs += (abs) ? fabs(r*expo->gamma) : r*expo->gamma;
  return rs;
}

/************************************************************/ 

double matColSumSIR(int i, int r, int abs) {
  if (i < expo->ki) return 0.0;
  int z = sirmat(i,r);
  double l = expo->lambda(i+r);
  double cs = -i*(l+expo->mu+expo->psi) - expo->shift;
  double x;
  if (abs) cs = fabs(cs);
  if (i > expo->ki) {
    x = (i-1+expo->ki)*expo->lambda(i-1+r);
    cs += (abs) ? fabs(x) : x;
  }
  if (i < expo->N-r && r > 0) {
    x = ((i+1)-expo->ki)*expo->mu;
    cs += (abs) ? fabs(x) : x;
  }
  if (r < expo->N-1) {
    x = (r+1)*expo->gamma;
    cs += (abs) ? fabs(x) : x;
  }
  return cs;
}

/************************************************************/ 
/* Matrix-vector product required for EXPMV                 */
/* - matrix-matrix product                                  */
/* - shifted matrix-matrix product                          */
/* - shifted matrix norm                                    */
/* - matrix trace                                           */
/************************************************************/ 

void matFuncExpmvShiftedSIR(char trans, int n1, int n2, 
                  double alpha, double* pin, double* pout) 
{
  int i, r, z;
  double l;
  // otherwise perform multiplication for ki <= m <=/expo->N N
  for (r = 0; r < expo->N; ++r) {
    for (i = 1; i <= expo->N-r; ++i) {
      z = sirmat(i,r);
      if (z < 0) {
        fprintf(stderr,"Bad row/column request!\n");
        abort();
      }
      if (i < expo->ki) pout[z] = 0.0;
      else {
        l = expo->lambda(i+r);
        pout[z] = (-i*(l+expo->mu+expo->psi)-expo->shift)*pin[z]; // p(I,R)
        if (i < expo->N-r)
          pout[z] += (i+expo->ki)*l*pin[z+1];                     // p(I+1,R)
        if (i > 1 && r < expo->N-1)
          pout[z] += (i-expo->ki)*expo->mu*pin[sirmat(i-1,r+1)];  // p(I-1,R+1)
        if (r > 0)
          pout[z] += r*expo->gamma*pin[sirmat(i,r-1)];           // p(I,R-1)
        if (alpha != 1.0) pout[z] *= alpha;
        if (isnan(pout[z])) pout[z] = 0.0;
      }
    }
  }
}

/************************************************************/ 

double matFuncOneNormSIR() {
  int i, r;
  double cs, max_cs;
  for (r = 0; r < expo->N; ++r) {
    for (i = expo->ki; i <= expo->N-r; ++i) {
      cs = matColSumSIR(i,r,1);
      if (cs > max_cs) max_cs = cs;
    }
  }
  return max_cs;
}

/************************************************************/ 

void transEventSIR(double* pin, double* pout) {
  int i, r;
  double l;
  for (r = 0; r < expo->N; ++r) {
    for (i = 1; i <= expo->N-r-1; ++i) {
      l = expo->lambda(i+r);
      pout[sirmat(i,r)] = 2*l*pin[sirmat(i+1,r)];
    }
    pout[sirmat(expo->N-r,r)] = 0.0;
  }
}

/************************************************************/ 

void sampleEventSIR(double* pin, double* pout) {
  int i, r;
  for (r = 0; r < expo->N-1; ++r) {
    pout[sirmat(0,r)] = 0.0;
    for (i = 1; i < expo->N-r; ++i) {
      pout[sirmat(i,r)] = expo->psi*pin[sirmat(i-1,r+1)];
    }
  }
  pout[sirmat(1,expo->N)] = 0.0;
}

#endif // __EXPOSIR_H_
