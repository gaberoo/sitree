#include "expoTree.h"

double expoTreeEval(int N, double beta, double mu, double psi, double rho,
    vector<double>& times, vector<int>& ttypes, 
    int extant, int SI, int vf, int rs)
{
  double fx = -INFINITY;

  if (beta <= 0.0 || mu < 0.0 || psi < 0.0 || rho < 0.0 || rho > 1.0) {
    if (vf > 0) fprintf(stderr,"Ilegal parameters. Returning inf.\n");
  } else {
    int ki = 0;
    int nt = times.size();
    double t0 = 0.0;
    double* ptimes = NULL;
    int*    pttypes = NULL;
    double* p0 = (double*) malloc((N+1)*sizeof(double));
    int root = (times[nt-1] == times[nt-2]) ? 1 : 0;
    // set initial value of p
    if (extant == 0) {
      p0[0] = 0.0;
      for (int m = 1; m <= N; ++m) p0[m] = psi;
      ki = 1;
      t0 = times[0];
      ptimes = times.data()+1;
      pttypes = ttypes.data()+1;
      nt = times.size()-1;
    } else {
      ki = extant;
      p0[0] = 0.0;
      for (int m = 1; m <= N; ++m) {
        if (m < extant) p0[m] = 0.0;
        else p0[m] = pow(rho,1.*extant)*pow(1.-rho,m-extant);
      }
      ptimes = times.data();
      pttypes = ttypes.data();
      nt = times.size();
    }
    rExpoTree(&N,&ki,&beta,&mu,&psi,&nt,ptimes,pttypes,p0,&t0,&SI,&vf,&rs);
    fx = p0[1];
    if (root) fx -= M_LN2 + log(beta) + log(1.-1./N);
    if (vf > 0) fprintf(stderr,"ln(p(1,t)) = %20.12e\n",fx);
    free(p0);
    p0 = NULL;
  }

  return fx;
}

// ===========================================================================

double expoTreeSurvival(int N, double beta, double mu, double psi, double rho,
    double torig, int extant, int SI, int vf, int rs)
{
  double fx = -INFINITY;

  if (beta <= 0.0 || mu < 0.0 || psi < 0.0 || rho < 0.0 || rho > 1.0) {
    if (vf > 0) fprintf(stderr,"Ilegal parameters. Returning inf.\n");
  } 
  
  else {
    int ttype = 1;
    double t0 = 0.0;
    double* p0 = (double*) malloc((N+1)*sizeof(double));
    // int root = 1;
    int ki = 0;
    int nt = 1;
    int i = 0;
    if (rho == 0.0) {
      for (i = 0; i <= N; ++i) p0[i] = 1.0;
      rExpoTree(&N,&ki,&beta,&mu,&psi,&nt,&torig,&ttype,p0,&t0,&SI,&vf,&rs);
      fx = p0[1];
      if (vf > 0) fprintf(stderr,"p(no samp) = %20.12e\n",exp(fx));
      fx = 1.-exp(fx);
    } else {
      for (i = 0; i <= N; ++i) {
        if (i < extant) p0[i] = 1.0;
        //else p0[i] = pow(rho,1.*extant)*pow(1.-rho,i-extant);
        else p0[i] = 0.0;
      }
      rExpoTree(&N,&ki,&beta,&mu,&psi,&nt,&torig,&ttype,p0,&t0,&SI,&vf,&rs);
      fx = 1-exp(p0[1]);
      // if (vf > 0) fprintf(stderr,"p(%d samples at t=0) = %20.12e\n",extant,fx);
    }
    free(p0);
    p0 = NULL;
  }

  return (fx > 0) ? log(fx) : -INFINITY;
}

// ===========================================================================

void readTimes(string fn, vector<double>& times, vector<int>& ttypes,
    int& extant, int& maxExtant) {
  ifstream in(fn.c_str());
  if (! in.is_open()) {
    cerr << "Problem opening file '" << fn << "'. Aborting." << endl;
    return;
  }
  string input;
  double x1;
  int    x2;
  while (! getline(in,input).eof()) {
    if (input.length() == 0) continue;
    if (input[0] == '#') continue;
    istringstream istr(input);
    istr >> x1 >> x2;
    times.push_back(x1);
    ttypes.push_back(x2);
  }
  in.close();
  // make sure times are sorted
  vector<size_t> p(times.size());
  gsl_sort_index(p.data(),times.data(),1,times.size());
  // apply sort
  gsl_permute(p.data(),times.data(),1,times.size());
  gsl_permute_int(p.data(),ttypes.data(),1,ttypes.size());
  extant = 0;
  maxExtant = 0;
  int n(times.size());
  for (int i(n-1); i >= 0; --i) {
    if (ttypes[i] == 0) --extant;
    else ++extant;
    if (maxExtant < extant) maxExtant = extant;
  }
  if (extant < 0) {
    fprintf(stderr,"Invalid tree: More samples than infections.\n");
    abort();
  } 
}

// ===========================================================================

double infTreeSurvival(double beta, double mu, double psi, double rho, double torig) 
{
  if (beta <= 0.0 || mu < 0.0 || psi < 0.0) return -INFINITY;
  double c1;
  double c2;
  double p0;
  c1 = beta-mu-psi;
  c1 = sqrt(c1*c1 + 4*beta*psi);
  c2 = -(beta-mu-psi-2*beta*rho)/c1;
  p0 = exp(-c1*torig)*(1.-c2);
  p0 = beta+mu+psi+c1*(p0-(1.+c2))/(p0+1.+c2);
  p0 = p0/(2.*beta);
  p0 = 1-p0;
  return log((p0 >= 0) ? ((p0 <= 1) ? p0 : 1) : 0);
}

// ===========================================================================

double infTreeEval(double beta, double mu, double psi, double rho,
    const vector<double>& times, const vector<int>& ttypes, 
    int extant, int SI, int survival, int vf) 
{
  if (beta <= 0.0 || mu < 0.0 || psi < 0.0) return -INFINITY;

  double c1;
  double c2;
  double lik;
  double p0;

  c1  = beta-mu-psi;
  c1  = sqrt(c1*c1 + 4*beta*psi);
  c2  = -(beta-mu-psi-2*beta*rho)/c1;
  lik = -log(2*beta);
  if (survival) {
    p0 = exp(-c1*times.back())*(1.-c2);
    p0 = beta+mu+psi+c1*(p0-(1.+c2))/(p0+1.+c2);
    p0 = p0/(2.*beta);
    lik -= log(1.-p0);
  }

  if (extant > 0) lik += extant*log(rho);
  for (size_t i(0); i < times.size(); ++i) {
    if (ttypes[i]) lik += log(2*beta/bdss_q(times[i],c1,c2));
    else lik += log(psi*bdss_q(times[i],c1,c2));
  }

  return lik;
}


