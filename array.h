#ifndef __ARRAY_H__
#define __ARRAY_H__

template<typename T>
class Array2D {
protected:
  size_t nx, ny;
  T* data;
public:
  Array2D(size_t x, size_t y) :
    nx(x), ny(y) 
  {
    data = (T*) malloc(x*y*sizeof(T));
  }
  virtual ~Array2D() { free(data); }
  inline T* pt(size_t x, size_t y) { return data + (x*ny + y); }
  inline T& operator()(size_t x, size_t y) { return data[x*ny + y]; }
  inline T& operator[](int i) { return data[i]; }
  inline void set_all(const T& val) { for (size_t i(0); i < nx*ny; ++i) data[i] = val; }
  inline size_t n_x() const { return nx; }
  inline size_t n_y() const { return ny; }
};

template<typename T>
class Array3D {
protected:
  size_t nx, ny, nz;
  T* data;
public:
  Array3D(size_t x, size_t y, size_t z) :
    nx(x), ny(y), nz(z) 
  {
    data = (T*) malloc(x*y*z*sizeof(T));
  }
  virtual ~Array3D() { free(data); }
  inline T* pt(size_t x, size_t y, size_t z) { return data + (x*ny*nz + y*nz + z); }
  inline T& operator()(size_t x, size_t y, size_t z) { return data[x*ny*nz + y*nz + z]; }
  inline void set_all(const T& val) { for (size_t i(0); i < nx*ny*nz; ++i) data[i] = val; }
  inline size_t n_x() const { return nx; }
  inline size_t n_y() const { return ny; }
  inline size_t n_z() const { return nz; }
};

#endif // __ARRAY_H__
