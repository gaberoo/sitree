#include <stdio.h>
#include "expmv.h"

int nn = 100;
int n = 10000;

double shifted_mnf() { return 4.0; }
double mtf() { return -4.0*n; }

void shifted_mmf(char trans, int n1, int n2, double alpha, double* x, double* y) {
  // equivalent to -gallery('poisson',10)
  int i = 0;
  for (i = 0; i < n; ++i) {
    y[i] = 0.0;
    if (i > 0 && (i % nn != 0)) y[i] += x[i-1];
    if (i >= nn) y[i] += x[i-nn];
    if (i < n-nn) y[i] += x[i+nn];
    if (i < n-1 && ((i+1) % nn != 0)) y[i] += x[i+1];
    y[i] *= alpha;
  }
}

int main() {
  double* b0 = (double*) malloc(n*sizeof(double));
  double* b  = (double*) malloc(n*sizeof(double));
  double* tm = (double*) malloc(7*55*sizeof(double));

  double t0 = 0.0;
  double tmax = 1.0;

  int i;

  for (i = 0; i < n; ++i) b[i] = -1.0+2.*i/(n-1.);
  memcpy(b0,b,n*sizeof(double));

  expmv(tmax,n,&shifted_mmf,&shifted_mnf,&mtf,b,1,55,8,tm,1,'d',1,0,0,1);

  for (i = 0; i < n; ++i)
    fprintf(stdout,"%3d %12.8f %12.8f\n",i+1,b0[i],b[i]);
  fflush(stdout);

  free(b);
  free(b0);
  free(tm);

  return 0;
}
