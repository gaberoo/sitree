#include <cstdlib>
#include <cmath>
#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <getopt.h>
using namespace std;

#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include "expoTree.h"
#include "Forest.h"

#ifdef SPRNG
#include "gsl-sprng.h"
#endif

#ifdef USE_MPI
#include "mpi.h"
#endif

// ===========================================================================

void printHelp();
int parseArgs(int argc, char** argv);

#ifdef USE_MPI
double lik_master(Forest& trees, const vector<double>& vars, 
                  int SImodel, int vflag, int rescale);
void lik_slave(Forest& trees, vector<double>& vars, 
               int SImodel, int vflag, int rescale);
#endif

// ===========================================================================

int Nmax = 1000;           /* maximum total population size */
vector<double> varInit(5,0.0);
vector<double> varLo(5,0.0);
vector<double> varHi(5,1.0);
int root = 0;              /* add a root to the tree */
int SImodel = 1;           /* use non-saturating model */
int n = 0;                 /* number of times */
int vflag = 0;             /* vebose flag */
int rescale = 1;           /* rescale probabilities in expomv */
int extant = 0;            /* number of extant species */
int maxExtant = 0;         /* max number of extant species */
int maxEvals = 10000;      /* max number of function evaluations */
int optimAlg = 1;          /* lock variables */
double fixedRatio = -1.0;  /* fix mu-phi ratio */
double alpha = 1e-2;

string fn = "";            /* input filename */
string out_fn = "";        /* output filename */

int mpi_rank = 0;
int mpi_ntasks = 0;

// ===========================================================================

int main(int argc, char* argv[]) {
  // =========================================================================

  if (parseArgs(argc,argv) == -1) return 0;
  if (optind == argc) {
    cerr << "Please provide at least one tree file.\n" << endl;
    printHelp();
    return 0;
  }

  // =========================================================================

  ostream* oout = &cout;
  if (mpi_rank == 0) {
    if (out_fn != "") oout = new ofstream(out_fn.c_str());
  }
  oout->setf(ios::scientific,ios::floatfield);
  oout->precision(12);

  Forest trees(argc-optind,argv+optind);

  double curLik;  /* current likelihood */
  double newLik;  /* proposal likelihood */

  vector<bool> lockVar(5);
  if ((optimAlg/16) % 2 == 1) lockVar[4] = true; // N
  if ((optimAlg/8)  % 2 == 1) lockVar[0] = true; // beta
  if ((optimAlg/4)  % 2 == 1) lockVar[1] = true; // mu
  if ((optimAlg/2)  % 2 == 1) lockVar[2] = true; // psi
  if ((optimAlg)    % 2 == 1) lockVar[3] = true; // rho

  if (fixedRatio >= 0.0) lockVar[1] = true;


  gsl_rng* rng = gsl_rng_alloc(gsl_rng_taus2);

  // GIBBS SAMPLER
  int i, j;

  int npars = 4;
  vector<double> curVar(npars);
  vector<double> newVar(npars);
  vector<double> sigma(npars,alpha);
  vector<int> accept(npars,0);

  varInit[3] = varLo[3];
  for (int i(0); i < npars; ++i) sigma[i] = alpha*varInit[i];
  if (fixedRatio >= 0.0) varInit[1] = varInit[2]*(1./fixedRatio-1.);

  curVar = varInit;
  newVar = curVar;

  curLik = 0.0;
  curLik = trees.jointInfLik(newVar,SImodel,vflag,rescale);
  *oout << "Inf " << curVar[0] << " " << curVar[1] << " " 
       << curVar[2] << " " << curVar[3] << " " << curLik << endl;
  for (i = 0; i < maxEvals; ++i) {
    for (j = 0; j < npars; ++j) {
      if (! lockVar[j]) {
        newVar[j] = gsl_ran_gaussian(rng,sigma[j]) + curVar[j];
        if (newVar[j] < varLo[j] || newVar[j] > varHi[j]) {
          newVar = curVar;
        } 
        if (fixedRatio >= 0.0) newVar[1] = newVar[2]*(1./fixedRatio-1.);
        newLik = trees.jointInfLik(newVar,SImodel,vflag,rescale);
        if (log(gsl_rng_uniform(rng)) < newLik-curLik) {
          curVar = newVar;
          curLik = newLik;
          ++accept[j];
        } else {
          newVar = curVar;
        }
      }
    }
    // sample new tree
    *oout << "Inf " << curVar[0] << " " << curVar[1] << " " 
          << curVar[2] << " " << curVar[3] << " " << curLik << endl;
  }

  // print diagnostic information
  cerr << "Acceptance ratios:" << endl;
  for (int j = 0; j < npars; ++j) 
    cerr << j << " " << 1.*accept[j]/maxEvals << endl;

  gsl_rng_free(rng);

  if (oout != &cout) delete oout;

  return EXIT_SUCCESS;
}

// ===========================================================================

void printHelp() {
  printf("mcmc: Markov chain Monte Carlo for density dependent trees\n\n");
  printf("usage: mcmc -N <population_size> -b <beta> -u <mu> -s <psi>\n");
  printf("               -l <SImodel> -f <times_file>\n");
  printf("               [-rvh]\n\n");
  printf("  N : starting population size\n");
  printf("  b : starting infection rate 'beta'\n");
  printf("  u : starting recovery rate 'mu'\n");
  printf("  r : rescale staring vector after each iteration\n");
  printf("  s : starting sampling rate 'psi'\n");
  printf("  l : model to use (0 = density independent; 1 = density dependent)\n");
  printf("  f : file with time samples\n");
  printf("  t : fixed parameters\n");
  printf("  v : verbose (can be used multiple times)\n");
  printf("  h : print this help message\n");
}

// ===========================================================================

int parseArgs(int argc, char** argv) {
  int c;
  opterr = 0;
  while (1) {
    static struct option long_options[] = {
      {"verbose",     no_argument,       0, 'v'},
      {"popSize",     required_argument, 0, 'N'},
      {"maxPop",      required_argument, 0, 'M'},
      {"model",       required_argument, 0, 'l'},
      {"betaLo",      required_argument, 0, 'b'},
      {"betaHi",      required_argument, 0, 'B'},
      {"muLo",        required_argument, 0, 'u'},
      {"muHi",        required_argument, 0, 'U'},
      {"psiLo",       required_argument, 0, 's'},
      {"psiHi",       required_argument, 0, 'S'},
      {"rhoLo",       required_argument, 0, 'o'},
      {"rhoHi",       required_argument, 0, 'O'},
      {"fixedRatio",  required_argument, 0, 'R'},
      {"maxEval",     required_argument, 0, 'e'},
      {"lockPar",     required_argument, 0, 't'},
      {"rescale",     required_argument, 0, 'r'},
      {"help",        no_argument,       0, 'h'},
      {"output",      required_argument, 0, 'C'},
      {"swarmSize",   required_argument, 0, 'w'},
      {"alpha",       required_argument, 0, 'a'},
      {0, 0, 0, 0}
    };
    int option_index = 0;
    c = getopt_long(argc,argv,"N:l:vb:B:u:U:s:S:o:O:R:e:t:hC:y:Y:z:Z:a:",
        long_options,&option_index);
    if (c == -1) break;
    switch (c) {
      case 'N': Nmax = atoi(optarg); break;
      case 'l': SImodel = atoi(optarg); break;
      case 'v': ++vflag; break;
      case 'b': varLo[0] = atof(optarg); break;
      case 'B': varHi[0] = atof(optarg); break;
      case 'u': varLo[1] = atof(optarg); break;
      case 'U': varHi[1] = atof(optarg); break;
      case 's': varLo[2] = atof(optarg); break;
      case 'S': varHi[2] = atof(optarg); break;
      case 'o': varLo[3] = atof(optarg); break;
      case 'O': varHi[3] = atof(optarg); break;
      case 'R': fixedRatio = atof(optarg); break;
      case 'e': maxEvals = atoi(optarg); break;
      case 'r': rescale = atoi(optarg); break;
      case 't': optimAlg = atoi(optarg); break;
      case 'C': out_fn = optarg; break;
      case 'h': printHelp(); return -1;
      case 'y': varInit[0] = atof(optarg); break;
      case 'Y': varInit[1] = atof(optarg); break;
      case 'z': varInit[2] = atof(optarg); break;
      case 'Z': varInit[4] = atof(optarg); break;
      case 'a': alpha = atof(optarg); break;

      default: break;
    }
  }

  return 0;
}

// ===========================================================================

#ifdef USE_MPI

double lik_master(Forest& trees, const vector<double>& vars, 
                  int SImodel, int vflag, int rescale) {
  double fx(0.0);
  double f(0.0);
  int id(0);
  MPI::Status status;
  int flag;
  int src;
  int idle(0);
  int k(0);
  size_t j(0);
  for (k = 1; k < mpi_ntasks && j < trees.size(); ++k) {
    MPI::COMM_WORLD.Send(&j,1,MPI::INT,k,1);
    MPI::COMM_WORLD.Send(vars.data(),vars.size(),MPI::DOUBLE,k,1);
    if (vflag) fprintf(stderr,"Sending tree %d to process %d.\n",(int) j,k);
    ++j;
  }
  while (1) {
    flag = MPI::COMM_WORLD.Iprobe(MPI::ANY_SOURCE,MPI::ANY_TAG,status);
    if (flag) {
      src = status.Get_source();
      MPI::COMM_WORLD.Recv(&id,1,MPI::INT,MPI::ANY_SOURCE,MPI::ANY_TAG,status);
      MPI::COMM_WORLD.Recv(&f,1,MPI::DOUBLE,src,MPI::ANY_TAG,status);
      if (vflag) fprintf(stderr,"Receiving tree %d from process %d.\n",id,src);
      fx += f;
      if (j < trees.size()) {
        MPI::COMM_WORLD.Send(&j,1,MPI::INT,src,1);
        MPI::COMM_WORLD.Send(vars.data(),vars.size(),MPI::DOUBLE,src,1);
        ++j;
      } else {
        ++idle;
      }
      if (idle == mpi_ntasks-1) break;
    }
  }

  return fx;
}

// ===========================================================================

void lik_slave(Forest& trees, vector<double>& vars, 
                  int SImodel, int vflag, int rescale) {
  double f(0.0);
  int id(0);
  MPI::Status status;
  while (1) {
    MPI::COMM_WORLD.Recv(&id,1,MPI::INT,0,MPI::ANY_TAG,status);
    if (status.Get_tag() == 0) break;
    else {
      MPI::COMM_WORLD.Recv(vars.data(),vars.size(),MPI::DOUBLE,0,MPI::ANY_TAG,status);
      f = trees.at(id)->likelihood(vars,SImodel,vflag,rescale);
      MPI::COMM_WORLD.Send(&id,1,MPI::INT,0,2);
      MPI::COMM_WORLD.Send(&f,1,MPI::DOUBLE,0,2);
    }
  }
  fprintf(stderr,"Slave %d done.\n",mpi_rank);
}

#endif
