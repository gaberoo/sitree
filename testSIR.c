#include "expo.h"
#include "expoSIR.h"

static expo_type* expo;

int main() {
  int r, i;

  expo = (expo_type*) malloc(sizeof(expo_type));
  init_expo(expo);

  expo->N = 10;
  expo->beta = 1.0;
  expo->mu = 0.1;
  expo->psi = 0.02;
  expo->gamma = 0.3;
  expo->ki = 2;
  expo->lambda = &lambdaSI;
  expo->shift = 10.0;

//  double rs, cs;
//  for (r = 0; r < expo->N; ++r) {
//    for (i = 1; i <= expo->N-r; ++i) {
//      rs = matRowSumSIR(i,r);
//      cs = matColSumSIR(i,r);
//      printf("%d %d %6.2f %6.2f\n",i,r,rs,cs);
//    }
//  }

  int Nmat = expo->N*(expo->N+1) / 2;
  printf("%f\n",matFuncOneNormSIR());
  expo->shift = matFuncTraceSIR()/Nmat;
  printf("%f\n",expo->shift);
  printf("%f\n",matFuncOneNormSIR());

  return 0;
}

