include Make.inc

##############################################################################

PROGS = testLik calc_likelihood sim_trees

##############################################################################

DLACN1 = cexpmv/dlacn1.a
EXPMV = cexpmv/expmv.a $(DLACN1) 

PSO_DIR = pso
PSO = $(PSO_DIR)/libpso.a
MPIPSO = $(PSO_DIR)/libpso_mpi.a

##############################################################################

all: calc_likelihood dream

clean:
	rm -f *.o; 
	rm -f $(RLIB_NAME);

distclean:
	cd cexpmv; make distclean; cd ..;
	rm -f *.o; 
	rm -f $(RLIB_NAME);

#$(OBJS):
#	cd $(OBJ_PATH); make;

##############################################################################

$(PSO): $(PSO_DIR)/*.h $(PSO_DIR)/*.cpp
	cd pso; make; cd ..;

$(EXPMV): cexpmv/*.h cexpmv/*.c
	cd cexpmv; make; cd ..;

expomv.o: expomv.c expo.h
	$(CC) $(CFLAGS) -c expomv.c

r_sim_trees.o: sim_trees.cpp
	$(CPP) $(CPPFLAGS) -DRLIB -c -o $@ $^

##############################################################################

testLik: testLik.o expoTree.o expomv.o $(EXPMV)
	$(LD) -o $@ $^ -lgsl $(LDFLAGS)

sim_trees: sim_trees.o
	$(LD) -o $@ $^ -lmygsl -lgsl $(LDFLAGS)

sim_epi: sim_epi.cpp
	$(CPP) $(CPPFLAGS) -o $@ $^ -lgsl $(LDFLAGS)

calc_likelihood: calc_likelihood.cpp expoTree.o expomv.o $(EXPMV) Forest.h Tree.h
	$(LD) -o $@ $^ $(LDFLAGS) -lgsl

optim_pso: optim_pso.o expomv.o expoTree.o $(EXPMV) TreeSwarm.o $(PSO)
	$(CPP) $(CPPFLAGS) -I$(PSO_DIR) -o $@ $^ -lgsl $(LDFLAGS)

optim_pso_inf: optim_pso.cpp expomv.o expoTree.o $(EXPMV) $(PSO) TreeSwarm.cpp
	$(CPP) $(CPPFLAGS) -I$(PSO_DIR) -DINF_N -o $@ $^ -lgsl $(LDFLAGS)

optim_pso_mpi: optim_pso.cpp expomv.o expoTree.o $(EXPMV) TreeSwarm.cpp $(MPIPSO)
	$(MPICPP) $(CPPFLAGS) -DUSE_MPI -I$(PSO_DIR) -o $@ $^ -lgsl $(MPILDFLAGS)

mcmc: mcmc.cpp expomv.o expoTree.o $(EXPMV)
	$(CPP) $(CFLAGS) -o $@ $^ -lgsl $(LDFLAGS)

mcmc_mpi: mcmc.cpp expomv.o expoTree.o $(EXPMV)
	$(MPICPP) -DUSE_MPI $(CFLAGS) -o $@ $^ -lgsl $(LDFLAGS) $(MPILDFLAGS)

mcmc_inf: expomv.o expoTree.o $(EXPMV) mcmc_inf.o
	$(LD) -o $@ $^ -lgsl $(LDFLAGS)

dream: dream.cpp expomv.o expoTree.o $(EXPMV)
	$(CPP) $(CFLAGS) -o $@ $^ -lgsl $(LDFLAGS)

dream_mpi: dream.cpp expomv.o expoTree.o $(EXPMV)
	$(MPICPP) -DUSE_MPI $(CFLAGS) -o $@ $^ -lgsl $(LDFLAGS) $(MPILDFLAGS)

##############################################################################

.c.o:; $(CC) $(CFLAGS) -c $<

.f.o:; $(FC) $(FFLAGS) -c $<

.cpp.o:; $(CPP) $(CPPFLAGS) -c $<


