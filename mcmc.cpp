#include <cstdlib>
#include <cmath>
#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <getopt.h>
using namespace std;

#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include "expoTree.h"
#include "Forest.h"

#ifdef USE_MPI
#include "mpi.h"
#endif

// ===========================================================================

void printHelp();
int parseArgs(int argc, char** argv);

#ifdef USE_MPI
double lik_master(Forest& trees, const vector<double>& vars, char type = 'j',
                  int SImodel = 1, int vflag = 0, int rescale = 1);
void lik_slave(Forest& trees, vector<double>& vars, 
               int SImodel, int vflag, int rescale);
#endif

// ===========================================================================

int Nmax = 1000;           /* maximum total population size */
vector<double> varInit(5,0.0);
vector<double> varLo(5,0.0);
vector<double> varHi(5,1.0);
vector<double> sigmaInit(5,0.0);
int root = 0;              /* add a root to the tree */
int SImodel = 1;           /* use non-saturating model */
int n = 0;                 /* number of times */
int vflag = 0;             /* vebose flag */
int rescale = 1;           /* rescale probabilities in expomv */
int extant = 0;            /* number of extant species */
int maxExtant = 0;         /* max number of extant species */
int maxEvals = 10000;      /* max number of function evaluations */
int optimAlg = 1;          /* lock variables */
double fixedRatio = -1.0;  /* fix mu-phi ratio */
double alpha = -1.0;
char jointLikelihood = 'g';   /* forest likelihood type */

string fn = "";            /* input filename */
string out_fn = "";        /* output filename */
int appendFile = 0;        /* append output to file */
int report_interval = 1;   /* report interval for state */
int diagnostics = 0;       /* report diagnostics at the end of the run */

int burnIn = 0;            /* number of steps for which to run an adaptive proposal size */
double increaseStep = 1.01*1.01*1.01;
double decreaseStep = 1.01;

// MPI variables
int mpi_rank = 0;
int mpi_ntasks = 0;
clock_t calc_time = 0;
clock_t run_time = 0;
clock_t last_time = 0;

// ===========================================================================

int main(int argc, char* argv[]) {
#ifdef USE_MPI
  MPI::Init(argc,argv);
  mpi_rank = MPI::COMM_WORLD.Get_rank();
  mpi_ntasks = MPI::COMM_WORLD.Get_size();
#endif

  // =========================================================================

  if (parseArgs(argc,argv) == -1) return 0;
  if (optind == argc) {
    cerr << "Please provide at least one tree file.\n" << endl;
    printHelp();
    return 0;
  }

  // =========================================================================

  ostream* oout = &cout;
  if (mpi_rank == 0) {
    if (out_fn != "" && out_fn != "-") {
      if (! appendFile) {
        oout = new ofstream(out_fn.c_str());
      }
      else oout = new ofstream(out_fn.c_str(),ios_base::app);
    }
  }
  oout->setf(ios::scientific,ios::floatfield);
  oout->precision(12);

  Forest trees(argc-optind,argv+optind);

  double curLik = -INFINITY;  /* current likelihood */
  double newLik = -INFINITY;  /* proposal likelihood */

  vector<bool> lockVar(5);
  if ((optimAlg/16) % 2 == 1) lockVar[4] = true; // N
  if ((optimAlg/8)  % 2 == 1) lockVar[0] = true; // beta
  if ((optimAlg/4)  % 2 == 1) lockVar[1] = true; // mu
  if ((optimAlg/2)  % 2 == 1) lockVar[2] = true; // psi
  if ((optimAlg)    % 2 == 1) lockVar[3] = true; // rho

  if (fixedRatio >= 0.0) lockVar[1] = true;

  gsl_rng* rng;
  rng = gsl_rng_alloc(gsl_rng_taus2);

  // GIBBS SAMPLER
  int i, j;

  int numPars (5);
  vector<double> curVar(numPars);
  vector<double> newVar(numPars);
  vector<double> sigma(sigmaInit);
  vector<int> accept(numPars,0);

  varInit[3] = varLo[3];
  if (fixedRatio >= 0.0) varInit[1] = varInit[2]*(1./fixedRatio-1.);

  varLo[4] = trees.maxExtant;
  varHi[4] = Nmax;
  if (alpha > 0.0) {
    for (int i(0); i < numPars; ++i) sigma[i] = alpha*varInit[i];
    sigma[4] = GSL_MAX(10*alpha*trees.maxExtant,1.0);
  }

  curVar = varInit;
  newVar = curVar;

  int ireport = 0;
  int acceptStep = 0;

  if (mpi_rank == 0) {
    if (! appendFile) {
      *oout << "# =======================================\n"
            << "# Metropolis-Hastings MCMC for likelihood\n"
            << "# =======================================\n";
      *oout << "# N        = [" << varLo[4] << "," << varHi[4] << "], sigma = " << sigma[4] << "\n"
            << "# beta     = [" << varLo[0] << "," << varHi[0] << "], sigma = " << sigma[0] << "\n"
            << "# mu       = [" << varLo[1] << "," << varHi[1] << "], sigma = " << sigma[1] << "\n"
            << "# psi      = [" << varLo[2] << "," << varHi[2] << "], sigma = " << sigma[2] << "\n"
            << "# rho      = [" << varLo[3] << "," << varHi[3] << "], sigma = " << sigma[3] << "\n"
            << "# root     = " << root << "\n"
            << "# SImodel  = " << SImodel << "\n"
            << "# rescale  = " << rescale << "\n"
            << "# maxEvals = " << maxEvals << "\n"
            << "# lockPar  = " << optimAlg << "\n"
            << "# fixRatio = " << fixedRatio << "\n"
            << "# outfile  = " << out_fn << "\n" 
            << "# burnIn   = " << burnIn << "\n"
            << "# joinType = " << jointLikelihood << "\n"
            << "# times files:\n";
      for (size_t t(0); t < trees.size(); ++t) *oout << "#   " << trees.at(t)->fn << "\n";
      *oout << "# \n"
            << "# extant species at time zero = " << extant << "\n"
            << "# max extant species = " << trees.maxExtant << endl;
    }

    curLik = 0.0;
#ifdef USE_MPI
    curLik = lik_master(trees,newVar,jointLikelihood,SImodel,vflag,rescale);
#else
    switch (jointLikelihood) {
      case 'm':
        curLik = trees.meanLikelihood(newVar,SImodel,vflag,rescale);
        break;
      case 'j':
        curLik = trees.jointLikelihood(newVar,SImodel,vflag,rescale);
        break;
      case 's':
        curLik = trees.sumLikelihood(newVar,SImodel,vflag,rescale);
        break;
      case 'g':
      default:
        curLik = trees.jointLikelihood(newVar,SImodel,vflag,rescale)/trees.size();
        break;
    }
#endif
    *oout << (int) curVar[4] << " " << curVar[0] << " " << curVar[1] << " " 
          << curVar[2] << " " << curVar[3] << " " << curLik << " "
          << sigma[4] << " " << sigma[0] << " " << sigma[1] << " " << sigma[2]
          << endl;    
    for (i = 0; i < maxEvals; ++i) {
      for (j = 0; j < 5; ++j) {
        if (! lockVar[j]) {
          acceptStep = 0;
          newVar[j] = gsl_ran_gaussian(rng,sigma[j]) + curVar[j];

          if (newVar[j] < varLo[j] || newVar[j] > varHi[j]) acceptStep = 0;
          else if (newVar[j] == curVar[j]) acceptStep = 1;
          else {
            if (fixedRatio >= 0.0) newVar[1] = newVar[2]*(1./fixedRatio-1.);
#ifdef USE_MPI
            newLik = lik_master(trees,newVar,jointLikelihood,SImodel,vflag,rescale);
#else
            switch (jointLikelihood) {
              case 'm':
                newLik = trees.meanLikelihood(newVar,SImodel,vflag,rescale);
                break;
              case 'j':
                newLik = trees.jointLikelihood(newVar,SImodel,vflag,rescale);
                break;
              case 's':
                newLik = trees.sumLikelihood(newVar,SImodel,vflag,rescale);
                break;
              case 'g':
              default:
                newLik = trees.jointLikelihood(newVar,SImodel,vflag,rescale)/trees.size();
                break;
            }
#endif
            if (newLik > curLik) acceptStep = 1;
            else {
              if (log(gsl_rng_uniform(rng)) < newLik-curLik) acceptStep = 1;
              else acceptStep = 0;
            }
          }

          if (acceptStep) {
            curVar = newVar;
            curLik = newLik;
            ++accept[j];
            if (i < burnIn) sigma[j] *= increaseStep;
          } else {
            newVar = curVar;
            if (i < burnIn) sigma[j] /= decreaseStep;
          }
        }
      }
      ++ireport;
      if (ireport >= report_interval) {
        ireport = 0;
        *oout << (int) curVar[4] << " " 
              << curVar[0] << " " 
              << curVar[1] << " " 
              << curVar[2] << " " 
              << curVar[3] << " " 
              << curLik << " "
              << sigma[4] << " " 
              << sigma[0] << " " 
              << sigma[1] << " " 
              << sigma[2] << endl;
      }
    }
#ifdef USE_MPI
    for (int k = 1; k < mpi_ntasks; ++k) {
      if (vflag) fprintf(stderr,"Sending done signal to process %d.\n",k);
      MPI::COMM_WORLD.Send(0,0,MPI::INT,k,0);
    }
#endif
    if (diagnostics) {
      // print diagnostic information
      *oout << "# Acceptance ratios:" << endl;
      for (int j = 0; j < numPars; ++j) 
        *oout << "# " << j << " " << fixed << 1.*accept[j]/maxEvals << endl;
    }
  } else {
#ifdef USE_MPI
    lik_slave(trees,newVar,SImodel,0,rescale);
#endif
  }

  gsl_rng_free(rng);

  if (oout != &cout) delete oout;

#ifdef USE_MPI
  last_time = clock();
  fprintf(stdout,"Calcluation efficiency (Slave %d): %f\n",
          mpi_rank,1.*calc_time/last_time);
  MPI::Finalize();
#endif

  return EXIT_SUCCESS;
}

// ===========================================================================

void printHelp() {
  printf("mcmc: Markov chain Monte Carlo for density dependent trees\n\n");
  printf("usage: mcmc -N <population_size> -b <beta> -u <mu> -s <psi>\n");
  printf("               -l <SImodel> -f <times_file>\n");
  printf("               [-rvh]\n\n");
  printf("  N : starting population size\n");
  printf("  b : starting infection rate 'beta'\n");
  printf("  u : starting recovery rate 'mu'\n");
  printf("  r : rescale staring vector after each iteration\n");
  printf("  s : starting sampling rate 'psi'\n");
  printf("  l : model to use (0 = density independent; 1 = density dependent)\n");
  printf("  f : file with time samples\n");
  printf("  t : fixed parameters\n");
  printf("  v : verbose (can be used multiple times)\n");
  printf("  h : print this help message\n");
}

// ===========================================================================

int parseArgs(int argc, char** argv) {
  int c;
  opterr = 0;
  int initVarNum = -1;
  int sigmaNum = -1;
  while (1) {
    static struct option long_options[] = {
      {"verbose",     no_argument,       0, 'v'},
      {"popSize",     required_argument, 0, 'N'},
      {"maxPop",      required_argument, 0, 'M'},
      {"model",       required_argument, 0, 'l'},
      {"betaLo",      required_argument, 0, 'b'},
      {"betaHi",      required_argument, 0, 'B'},
      {"muLo",        required_argument, 0, 'u'},
      {"muHi",        required_argument, 0, 'U'},
      {"psiLo",       required_argument, 0, 's'},
      {"psiHi",       required_argument, 0, 'S'},
      {"rhoLo",       required_argument, 0, 'o'},
      {"rhoHi",       required_argument, 0, 'O'},
      {"fixedRatio",  required_argument, 0, 'R'},
      {"maxEval",     required_argument, 0, 'e'},
      {"lockPar",     required_argument, 0, 't'},
      {"rescale",     required_argument, 0, 'r'},
      {"help",        no_argument,       0, 'h'},
      {"output",      required_argument, 0, 'C'},
      {"swarmSize",   required_argument, 0, 'w'},
      {"alpha",       required_argument, 0, 'a'},
      {"report",      required_argument, 0, 'I'},
      {"diagnostics", no_argument,       0, 'D'},
      {"burnIn",      required_argument, 0, 'X'},
      {"jointType",   required_argument, 0, 'J'},
      {"append",      no_argument,       0, 'A'},
      {"initVarNum",  required_argument, 0, 'y'},
      {"initVarVal",  required_argument, 0, 'Y'},
      {"sigmaNum",    required_argument, 0, 'z'},
      {"sigmaVal",    required_argument, 0, 'Z'},
      {0, 0, 0, 0}
    };
    int option_index = 0;
    c = getopt_long(argc,argv,"N:l:vb:B:u:U:s:S:o:O:R:e:t:hC:y:Y:z:Z:a:r:I:DX:J:A",
        long_options,&option_index);
    if (c == -1) break;
    switch (c) {
      case 'N': Nmax = atoi(optarg); break;
      case 'l': SImodel = atoi(optarg); break;
      case 'v': ++vflag; break;
      case 'b': varLo[0] = atof(optarg); break;
      case 'B': varHi[0] = atof(optarg); break;
      case 'u': varLo[1] = atof(optarg); break;
      case 'U': varHi[1] = atof(optarg); break;
      case 's': varLo[2] = atof(optarg); break;
      case 'S': varHi[2] = atof(optarg); break;
      case 'o': varLo[3] = atof(optarg); break;
      case 'O': varHi[3] = atof(optarg); break;
      case 'R': fixedRatio = atof(optarg); break;
      case 'e': maxEvals = atoi(optarg); break;
      case 'r': rescale = atoi(optarg); break;
      case 't': optimAlg = atoi(optarg); break;
      case 'C': out_fn = optarg; break;
      case 'h': printHelp(); return -1;
      case 'y': initVarNum = atoi(optarg); break;
      case 'Y': 
        if (initVarNum < 0) {
          cerr << "Please specify initial value index." << endl;
          return -1;
        }
        varInit[initVarNum] = atof(optarg); 
        initVarNum = -1;
        break;
      case 'z': sigmaNum = atoi(optarg); break;
      case 'Z':
        if (sigmaNum < 0) {
          cerr << "Please specify sigma value index." << endl;
          return -1;
        }
        sigmaInit[sigmaNum] = atof(optarg); 
        sigmaNum = -1;
        break;
      case 'a': alpha = atof(optarg); break;
      case 'I': report_interval = atoi(optarg); break;
      case 'D': ++diagnostics; break;
      case 'X': burnIn = atoi(optarg); break;
      case 'J': jointLikelihood = optarg[0]; break;
      case 'A': appendFile = 1; break;
      default: break;
    }
  }

  return 0;
}

// ===========================================================================

#ifdef USE_MPI

double lik_master(Forest& trees, const vector<double>& vars, char type,
                  int SImodel, int vflag, int rescale) 
{
  int n = (type == 'm') ? trees.size() : 1;
  double* fx = new double[n];
  double f(0.0);
  double mf(0.0);
  double f0(0.0);
  int n0(0);
  int id(0);
  MPI::Status status;
  int flag;
  int src;
  int idle(0);
  int k(0);
  size_t j(0);
  for (k = 0; k < n; ++k) fx[k] = 0.0;
  for (k = 1; k < mpi_ntasks && j < trees.size(); ++k) {
    MPI::COMM_WORLD.Send(&j,1,MPI::INT,k,1);
    MPI::COMM_WORLD.Send(vars.data(),vars.size(),MPI::DOUBLE,k,1);
    if (vflag) fprintf(stderr,"Sending tree %d to process %d.\n",(int) j,k);
    ++j;
  }
  while (1) {
    flag = MPI::COMM_WORLD.Iprobe(MPI::ANY_SOURCE,MPI::ANY_TAG,status);
    if (flag) {
      src = status.Get_source();
      MPI::COMM_WORLD.Recv(&id,1,MPI::INT,MPI::ANY_SOURCE,MPI::ANY_TAG,status);
      MPI::COMM_WORLD.Recv(&f,1,MPI::DOUBLE,src,MPI::ANY_TAG,status);
      if (vflag) fprintf(stderr,"Receiving tree %d from process %d.\n",id,src);
      if (j < trees.size()) {
        MPI::COMM_WORLD.Send(&j,1,MPI::INT,src,1);
        MPI::COMM_WORLD.Send(vars.data(),vars.size(),MPI::DOUBLE,src,1);
        ++j;
      } else {
        ++idle;
      }
      switch (type) {
        case 'M':
        case 'm': 
          fx[id] = f; 
          if (isfinite(f)) { f0 += f; ++n0; }
          break;
        case 'J':
        case 'j':
        case 'G':
        case 'g':
        default: 
          fx[0] += f; 
          break;
      }
      if (idle == mpi_ntasks-1) break;
    }
  }
  switch (type) {
    case 'm':
      f0 /= n0;
      mf = 0.0;
      for (j = 0; j < trees.size(); ++j) mf += exp(fx[j]-f0);
      mf = log(mf)+f0-log(trees.size());
      break;
    case 's':
      f0 /= n0;
      mf = 0.0;
      for (j = 0; j < trees.size(); ++j) mf += exp(fx[j]-f0);
      mf = log(mf)+f0;
      break;
    case 'j':
      mf = fx[0];
      break;
    case 'g':
    default:
      mf = fx[0]/(1.*trees.size());
      break;
  }
  delete[] fx;
  return mf;
}

// ===========================================================================

void lik_slave(Forest& trees, vector<double>& vars, 
                  int SImodel, int vflag, int rescale) {
  double f(0.0);
  int id(0);
  MPI::Status status;
  while (1) {
    MPI::COMM_WORLD.Recv(&id,1,MPI::INT,0,MPI::ANY_TAG,status);
    if (status.Get_tag() == 0) break;
    else {
      MPI::COMM_WORLD.Recv(vars.data(),vars.size(),MPI::DOUBLE,0,MPI::ANY_TAG,status);
      last_time = clock();
      f = trees.at(id)->likelihood(vars,SImodel,vflag,rescale);
      calc_time += clock()-last_time;
      MPI::COMM_WORLD.Send(&id,1,MPI::INT,0,2);
      MPI::COMM_WORLD.Send(&f,1,MPI::DOUBLE,0,2);
    }
  }
  fprintf(stderr,"Slave %d done.\n",mpi_rank);
}

#endif
